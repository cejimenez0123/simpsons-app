package com.horizontal.lib_data.data

import com.google.gson.GsonBuilder
import com.horizontal.lib_data.domain.models.CharacterListResult
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers

interface ApiService {
    @GET("/?q=simpsons+characters&format=json")
    suspend fun getSimpsonCharacterList(): Response<CharacterListResult>
    @GET("/?q=the+wire+characters&format=json")
    suspend fun getWireCharacterList():Response<CharacterListResult>

    companion object {
        private const val BASE_URL = "https://api.duckduckgo.com"
        val apiServiceInstance: ApiService by lazy {
           val gson = GsonBuilder()
                .setLenient()
                .create()
            Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(ScalarsConverterFactory.create()).addConverterFactory(GsonConverterFactory.create(gson)).build().create(
                ApiService::class.java)
        }
    }

}