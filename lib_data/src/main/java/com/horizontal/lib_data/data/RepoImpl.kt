package com.horizontal.lib_data.data

import android.util.Log
import com.horizontal.lib_data.domain.models.CharacterListResult
import com.horizontal.lib_data.domain.repository.Repository
import com.horizontal.lib_data.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


object Repo : Repository {
    private val apiServiceInstance by lazy {
        ApiService.apiServiceInstance
    }

    override suspend fun getSimpsonCharacterList(): Resource<CharacterListResult> =
        withContext((Dispatchers.IO)) {
            val res = apiServiceInstance.getSimpsonCharacterList()
            return@withContext try {

                if (res.isSuccessful && res.body() != null) {
//               val result = res.body()!!
                    Resource.Success(res.body()!!)
                } else {
                    Resource.Error("Something went wrong")
                }

            } catch (e: Exception) {
                Resource.Error(e.localizedMessage!!.toString())
            }

        }

    override suspend fun getWireCharacterList(): Resource<CharacterListResult> =
        withContext((Dispatchers.IO)) {
            val res = apiServiceInstance.getWireCharacterList()
            return@withContext try {

                if (res.isSuccessful && res.body() != null) {
                    Log.d("WireX", res.body().toString())
                    Resource.Success(res.body()!!)
                } else {
                    Resource.Error("Something went wrong")
                }

            } catch (e: Exception) {
                Log.e("Wire", e.localizedMessage.toString())
                Resource.Error(e.localizedMessage!!.toString())
            }

        }

}
