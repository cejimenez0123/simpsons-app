package com.horizontal.lib_data.domain.models

import com.horizontal.lib_data.domain.models.Character

data class Character(
    val FirstURL: String,
    val Icon: Icon,
    val Result: String,
    val Text: String
)
fun Character.name():String?{
    val regexPattern = "^([A-Za-z\\s]+)".toRegex()
    val matchResult = regexPattern.find(this.Text)
    return matchResult?.groupValues?.getOrNull(1)
}
