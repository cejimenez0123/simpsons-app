package com.horizontal.lib_data.domain.models

data class Developer(
    val name: String,
    val type: String,
    val url: String
)