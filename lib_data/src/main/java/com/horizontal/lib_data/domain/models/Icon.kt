package com.horizontal.lib_data.domain.models

data class Icon(
    val Height: String,
    val URL: String,
    val Width: String
)