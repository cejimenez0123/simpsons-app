package com.horizontal.lib_data.domain.models

data class Maintainer(
    val github: String
)