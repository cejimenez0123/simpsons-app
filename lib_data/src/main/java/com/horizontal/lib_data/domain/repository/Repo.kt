package com.horizontal.lib_data.domain.repository

import android.util.Log
import com.horizontal.lib_data.data.Repo
import com.horizontal.lib_data.domain.models.CharacterListResult
import com.horizontal.lib_data.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

interface Repository{
    suspend fun getSimpsonCharacterList(): Resource<CharacterListResult>
    suspend fun getWireCharacterList(): Resource<CharacterListResult>
}