package com.horizontal.lib_data.util

sealed class Resource<T>(data: T?, message: String? = null) {
    data class Success<T>(val data:T): Resource<T>(data)
    class Loading<T>: Resource<T>(null,"Loading")
    data class Error<T>(val message: String?) : Resource<T>(data = null,message)
}
