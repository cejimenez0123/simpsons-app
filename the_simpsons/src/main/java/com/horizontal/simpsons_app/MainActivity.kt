package com.horizontal.the_simpsons

import android.content.res.Configuration
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.horizontal.the_simpsons.presentations.SimpsonDetailFragment
import com.horizontal.the_simpsons.presentations.SimpsonListFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class MainActivity : AppCompatActivity() {
    private val detailFragment = SimpsonDetailFragment()
    private val listFragment = SimpsonListFragment()
    private val isTablet by lazy {
        ((resources.configuration.screenLayout
                and Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE || isTablet) {
            lifecycleScope.launch(Dispatchers.Main) {
                val fm = supportFragmentManager
                fm.beginTransaction().replace(R.id.fragment_detail, detailFragment).commit()
                fm.beginTransaction().replace(R.id.fragment_list, listFragment).commit()

            }
        }
    }
}