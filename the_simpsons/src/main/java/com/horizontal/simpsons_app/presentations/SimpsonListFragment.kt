package com.horizontal.the_simpsons.presentations

import android.content.res.Configuration
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.horizontal.lib_data.domain.models.Character
import com.horizontal.the_simposons.presentations.SimpsonMainViewModel
import com.horizontal.the_simpsons.databinding.FragmentSimpsonListBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.ensureActive
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch


class SimpsonListFragment : Fragment() {
    private var _binding: FragmentSimpsonListBinding? = null
    private val viewModel by activityViewModels<SimpsonMainViewModel>()
    private var charAdapter = SimpsonListAdapter(::navigateToDetail)
    private var job: Job? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentSimpsonListBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    fun initViews() = with(_binding) {
        job?.cancel()
        job = lifecycleScope.launch(Dispatchers.Main) {
            viewModel.characterFilteredList.collectLatest { list ->
                charAdapter.apply {
                    displayCharacterList(list)
                }
            }
            ensureActive()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding?.rv?.adapter = charAdapter
        _binding?.txtfield?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {

            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {

            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
                var searchJob: Job? = null
                searchJob?.cancel()
                searchJob = lifecycleScope.launch {
                    delay(7)
                    viewModel.searchCharacter(s.toString())
                }

            }
        })
        initViews()
    }

    private fun navigateToDetail(char: Character) {
        lifecycleScope.launch() {
            viewModel.setCharacterDetail(character = char)
        }
        val isTablet = ((resources.configuration.screenLayout
                and Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE)
        if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT && !isTablet) {
            val action =
                SimpsonListFragmentDirections.actionSimpsonListFragmentToSimpsonDetailFragment()
            findNavController().navigate(action)
        }
    }
}