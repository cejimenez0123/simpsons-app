package com.horizontal.the_simposons.presentations

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.horizontal.lib_data.data.Repo
import com.horizontal.lib_data.domain.models.Character
import com.horizontal.lib_data.domain.models.name
import com.horizontal.lib_data.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.ensureActive
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SimpsonMainViewModel : ViewModel() {
    private var _characterList = MutableStateFlow(listOf<Character>())
    val characterList = _characterList.asStateFlow()
    private var _characterFilteredList = MutableStateFlow(listOf<Character>())
    val characterFilteredList get() = _characterFilteredList
    private var _characterDetail = MutableStateFlow<Character?>(null)
    val characterDetail get() = _characterDetail
    private var job: Job?=null
    private val repo = Repo

    init {
       job = viewModelScope.launch(Dispatchers.IO) {
            getList()
        }
    }

    suspend fun setCharacterDetail(character: Character)= withContext(Dispatchers.Main) {
        _characterDetail.emit(character)
    }

    private suspend fun getList() = withContext(Dispatchers.IO) {
        ensureActive()
        when (val res = repo.getSimpsonCharacterList()) {

                    is Resource.Error -> {
                val e = res.message ?: "Error"
            }
            is Resource.Loading -> {

            }
            is Resource.Success -> {
                characterFilteredList.value = res.data.RelatedTopics
                _characterList.value = res.data.RelatedTopics
            }
        }
    }

    fun searchCharacter(query: String) {
        if (query.isNotBlank()) {
            val list = _characterList.value
            _characterFilteredList.value = list.filter { char ->
                val name = char.name()?.uppercase()
                name?.uppercase()?.contains(query.uppercase()) ?: false || char.Text.uppercase()
                    .contains(query.uppercase())
            }
        } else {
            _characterFilteredList.value = _characterList.value
        }
    }
}