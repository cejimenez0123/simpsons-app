package com.horizontal.the_wire

import android.content.res.Configuration
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.horizontal.the_wire.presentations.WireDetailFragment
import com.horizontal.the_wire.presentations.WireListFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    private val detailFragment = WireDetailFragment()
    private val listFragment = WireListFragment()
    private val isTablet by lazy {
        ((resources.configuration.screenLayout
                and Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE || isTablet) {
            lifecycleScope.launch(Dispatchers.Default) {
                val fm = supportFragmentManager
                fm.beginTransaction().replace(R.id.fragment_detail, detailFragment).commit()
                fm.beginTransaction().replace(R.id.fragment_list, listFragment).commit()
            }
        }
    }
    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        setContentView(R.layout.activity_main)

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE || isTablet) {
            lifecycleScope.launch(Dispatchers.Default) {
                val fm = supportFragmentManager
                fm.beginTransaction().replace(R.id.fragment_detail, detailFragment).commit()
                fm.beginTransaction().replace(R.id.fragment_list, listFragment).commit()
            }
        }
    }
}