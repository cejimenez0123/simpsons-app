package com.horizontal.the_wire.presentations

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import coil.load
import com.horizontal.lib_data.domain.models.name
import com.horizontal.the_wire.databinding.FragmentWireDetailBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import com.horizontal.the_wire.R
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest


class WireDetailFragment : Fragment() {
    private var _binding: FragmentWireDetailBinding? = null
    private val viewModel by activityViewModels<WireMainViewModel>()
    private var job: Job? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentWireDetailBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        job?.cancel()
       job = lifecycleScope.launch(Dispatchers.Main) {

            setView()
        }
    }
    private suspend fun setView() {
        viewModel.characterDetail.collectLatest { res ->
            if (res != null) {
                if (res.Icon.URL.isNotBlank()) {
                    _binding?.characterImage?.load("https://duckduckgo.com${res.Icon.URL}")
                } else {
                    activity?.baseContext?.let {
                        val drawable = ContextCompat.getDrawable(
                            it,
                            R.drawable.baseline_person_24
                        )
                        _binding?.characterImage?.setImageDrawable(drawable)
                    }
                }
                val name = res.name()
                if (name?.isNotBlank() == true) {
                    _binding?.characterTitle?.text =
                        res.name() ?: "Error: No Title, Omar took it"
                } else {
                    _binding?.characterTitle?.text = "Error: No Title, Omar took it"
                }
                if (res.Text.isNotBlank()) {
                    _binding?.characterDetails?.text = res.Text
                } else {
                    _binding?.characterDetails?.text = "No Info, Omar took It"
                }

            } else {
                activity?.baseContext?.let {
                    val drawable = ContextCompat.getDrawable(
                        it,
                        R.drawable.baseline_person_24
                    )
                    _binding?.characterImage?.setImageDrawable(drawable)
                }
                _binding?.characterTitle?.text = "No Title, Omar took it"
                _binding?.characterDetails?.text = "No Info, Omar took It"
            }
        }
    }
}