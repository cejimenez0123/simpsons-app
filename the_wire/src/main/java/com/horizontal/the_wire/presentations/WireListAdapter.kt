package com.horizontal.the_wire.presentations

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.horizontal.lib_data.domain.models.Character
import com.horizontal.lib_data.domain.models.name
import com.horizontal.the_wire.databinding.WireItemBinding


class WireListAdapter(private val navigate: (character:Character) -> Unit) :
    RecyclerView.Adapter<WireListAdapter.CharacterListViewHolder>() {
    private var characterList = listOf<Character>()

    class CharacterListViewHolder(private val binding: WireItemBinding,private val navigate: (character: Character) -> Unit) :
        RecyclerView.ViewHolder(binding.root) {

        fun attach(char: Character) {
            binding.run {
                characterTitle.text = char.name()
                root.setOnClickListener{
                    navigate(char)
                }
            }
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterListViewHolder {
        val binding = WireItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CharacterListViewHolder(binding, navigate = navigate)
    }

    fun displayCharacterList(charList: List<Character>) {
        this.characterList = charList
        notifyDataSetChanged()
    }
    override fun getItemCount(): Int {
        return characterList.size
    }
    override fun onBindViewHolder(holder: CharacterListViewHolder, position: Int) {
        holder.attach(characterList[position])
    }
}